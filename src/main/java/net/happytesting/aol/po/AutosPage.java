package net.happytesting.aol.po;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AutosPage {
  @FindBy(xpath = "//img[@alt='Autoblog Logo']")
  private WebElement logo;
  
  public boolean isLogoVisible() {
    return logo.isDisplayed();
  }
}
