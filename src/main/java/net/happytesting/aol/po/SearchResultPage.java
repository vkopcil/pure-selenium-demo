package net.happytesting.aol.po;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchResultPage {
  private final WebDriver driver;

  @FindBy(id = "result-count")
  private WebElement resultCount;

  @FindBy(id = "home")
  private WebElement homeLink;

  @FindBy(id = "mail")
  private WebElement mailLink;

  @FindBy(xpath = "//div[contains(@class,'MSL')]/ul/li")
  private List<WebElement> results;

  public SearchResultPage(WebDriver driver) {
    this.driver = driver;
  }

  public boolean isHomeLinkDisplayed() {
    return homeLink.isDisplayed();
  }

  public boolean isMailLinkDisplayed() {
    return mailLink.isDisplayed();
  }

  public Iterator<String> getResultLinkIterator() {
    return results.stream()
        .map(link -> link.findElement(By.tagName("a")).getAttribute("href"))
        .iterator();
  }

  public void openResultLink(int index) {
    results.get(index).findElement(By.tagName("a")).click();
  }

  public void waitUntilRendered() {
    new WebDriverWait(driver, 60).withMessage("waiting for search results")
        .until(ExpectedConditions.visibilityOf(resultCount));
  }
}
