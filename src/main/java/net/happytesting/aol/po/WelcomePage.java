package net.happytesting.aol.po;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.google.common.collect.ImmutableList;

public class WelcomePage {
  private final WebDriver driver;

  @FindBy(xpath = "//ul[@class='topLevel']//a[contains(@class, 'itemLink')]")
  private List<WebElement> menuLinks;

  @FindBy(id = "aol-header-query")
  private WebElement queryField;

  @FindBy(id = "aol-header-search-button")
  private WebElement searchButton;

  public WelcomePage(WebDriver driver) {
    this.driver = driver;
  }

  public List<String> getMenuTitles() {
    return ImmutableList.copyOf(menuLinks.stream().map(link -> link.getText())
        .iterator());
  }

  public boolean isMenuLinkVisible(int index) {
    return menuLinks.get(index).isDisplayed();
  }

  public SearchResultPage searchFor(String what) {
    queryField.sendKeys(what);
    searchButton.click();
    SearchResultPage nextPage = PageFactory.initElements(driver,
        SearchResultPage.class);
    nextPage.waitUntilRendered();
    return nextPage;
  }
}
