package net.happytesting.aol;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;
import java.util.Set;

import net.happytesting.aol.po.AutosPage;
import net.happytesting.aol.po.SearchResultPage;
import net.happytesting.aol.po.WelcomePage;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;

/**
 * 
 * @author vlad
 *
 *         Using Firefox go to http://www.aol.com
 * 
 *         Verify ‘NEWS’, ‘SPORTS’ and ‘ENTERTAINMENT’ links/elements are
 *         present on the page
 * 
 *         Then search for AOL Autos
 * 
 *         Verify that ‘Web’ and ‘Mail’ links/elements are present
 * 
 *         Then click on http://autos.aol.com/ search result
 * 
 *         and verify that the AOL Autos Logo is present from the resulting page
 */
public class PureSeleniumTest {
  private static WebDriver driver;
  private static WelcomePage welcomePage;

  private final String mandatoryTitles[] = { "NEWS", "SPORTS", "ENTERTAINMENT" };

  @BeforeClass
  public static void setup() {
    driver = new FirefoxDriver();
    driver.get("http://www.aol.com");
    welcomePage = PageFactory.initElements(driver, WelcomePage.class);
  }

  @AfterClass
  public static void tearDown() {
    if (driver != null) {
      driver.quit();
      driver = null;
    }
  }

  @Test
  public void demoTest() {
    List<String> menuTitles = welcomePage.getMenuTitles();
    for (String title : mandatoryTitles) {
      assertThat(menuTitles).contains(title);
      assertThat(welcomePage.isMenuLinkVisible(menuTitles.indexOf(title))).as(
          title + " link is visible").isTrue();
    }
    SearchResultPage searchResultPage = welcomePage.searchFor("AOL Autos");
    assertThat(searchResultPage.isHomeLinkDisplayed())
        .as("home link displayed").isTrue();
    assertThat(searchResultPage.isMailLinkDisplayed())
        .as("mail link displayed").isTrue();
    int autosIndex = Iterators.indexOf(
        searchResultPage.getResultLinkIterator(), new Predicate<String>() {

          @Override
          public boolean apply(String input) {
            return input.contentEquals("http://autos.aol.com/");
            // return input.startsWith("http://autos.aol.com/");
          }
        });
    assertThat(autosIndex >= 0).as("results contains link to autos.aol.com")
        .isTrue();
    Set<String> windows = driver.getWindowHandles();
    searchResultPage.openResultLink(autosIndex);
    new WebDriverWait(driver, 60).withMessage("waiting for new window").until(
        new Predicate<WebDriver>() {

          @Override
          public boolean apply(WebDriver driver) {
            return driver.getWindowHandles().size() > windows.size();
          }
        });
    for (String window : driver.getWindowHandles()) {
      if (!windows.contains(window)) {
        driver.switchTo().window(window);
        break;
      }
    }
    AutosPage autosPage = PageFactory.initElements(driver, AutosPage.class);
    assertThat(autosPage.isLogoVisible()).as("Autos Logo is present").isTrue();
  }
}
